﻿using empresas_dotNET.V1.Authentication.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace empresas_dotNET.V1.Authentication.Services
{

    public class AuthenticationService : IAuthenticationService
    {
        private readonly string authentication_api_url;

        public AuthenticationService(IConfiguration configuration) 
        {
            authentication_api_url = configuration.GetValue<string>("AuthenticationApi:url");
        }

        public async Task SignIn(string email, string password)
        {
            var httpClient = new HttpClient();

            var request = new HttpRequestMessage()
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri(authentication_api_url),
                Content = new StringContent("{" +
                    "\"email\":\""+ email +"\"," +
                    "\"password\":" + password + "}",
                    Encoding.UTF8, "application/json")
            };

            request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var response = await httpClient.SendAsync(request);

            // Check login success
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var accessToken = response.Headers.GetValues("access-token").FirstOrDefault();
                var client = response.Headers.GetValues("client").FirstOrDefault();
                var uid = response.Headers.GetValues("uid").FirstOrDefault();

                // Check header is not empty
                if (!string.IsNullOrEmpty(accessToken) && !string.IsNullOrEmpty(client) && !string.IsNullOrEmpty(uid))
                {
                    ResponseHeader.GetResponseHeader().IsAuthorized = true;
                    ResponseHeader.GetResponseHeader().AccessToken = response.Headers.GetValues("access-token").FirstOrDefault();
                    ResponseHeader.GetResponseHeader().Client = response.Headers.GetValues("client").FirstOrDefault();
                    ResponseHeader.GetResponseHeader().Uid = response.Headers.GetValues("uid").FirstOrDefault();
                }
            }
            else
            {
                ResponseHeader.GetResponseHeader().IsAuthorized = false;
                ResponseHeader.GetResponseHeader().AccessToken = null;
                ResponseHeader.GetResponseHeader().Client = null;
                ResponseHeader.GetResponseHeader().Uid = null;
            }
        }
    }
}
