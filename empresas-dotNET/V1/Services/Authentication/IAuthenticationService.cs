﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace empresas_dotNET.V1.Authentication.Services
{
    public interface IAuthenticationService
    {
        //Task<ResponseHeader> SignIn(string email, string password);
        Task SignIn(string email, string password);
    }
}
