﻿using empresas_dotNET.V1.Enterprise.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace empresas_dotNET.V1.Enterprise.Services
{
    public interface IEnterpriseService
    {
        IEnumerable<Models.Enterprise> FilterAsync(string name, Enterprise_Types type);
        Task<Models.Enterprise> GetEnterpriseAsync(int id);
        bool ExistsAsync(int id);
    }
}
