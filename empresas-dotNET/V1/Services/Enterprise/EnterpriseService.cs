﻿using empresas_dotNET.V1.Enterprise.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace empresas_dotNET.V1.Enterprise.Services

{
    public class EnterpriseService : IEnterpriseService
    {
        private readonly EnterpriseContext _context;

        public EnterpriseService(EnterpriseContext context)
        {
            _context = context;
        }

        public IEnumerable<Models.Enterprise> FilterAsync(string name, Enterprise_Types type)
        {
            if (type > 0 && string.IsNullOrEmpty(name))
                return _context.Enterprise.Where(e => e.Type == type);

            if (type <= 0 && !string.IsNullOrEmpty(name))
                return _context.Enterprise.Where(e => e.Name == name);

            if (type > 0 && !string.IsNullOrEmpty(name))
                return _context.Enterprise.Where(e => e.Name == name && e.Type == type);

            return _context.Enterprise;
        }

        public Task<Models.Enterprise> GetEnterpriseAsync(int id)
        {
            return _context.Enterprise.FindAsync(id);
        }

        public bool ExistsAsync(int id)
        {
            return _context.Enterprise.Any(e => e.Id == id);
        }
    }
}
