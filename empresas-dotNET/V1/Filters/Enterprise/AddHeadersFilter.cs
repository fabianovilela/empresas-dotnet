﻿using empresas_dotNET.V1.Authentication.Models;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace empresas_dotNET.V1.Enterprise.Filters
{
    public class AddHeaders : Attribute, IResourceFilter
    {
        public void OnResourceExecuting(ResourceExecutingContext context)
        {
            var accessToken = ResponseHeader.GetResponseHeader().AccessToken;
            var client = ResponseHeader.GetResponseHeader().Client;
            var uid = ResponseHeader.GetResponseHeader().Uid;

            context.HttpContext.Request.Headers.Add("access-token", accessToken);
            context.HttpContext.Request.Headers.Add("client", client);
            context.HttpContext.Request.Headers.Add("uid", uid);

            context.HttpContext.Response.Headers.Add("access-token", accessToken);
            context.HttpContext.Response.Headers.Add("client", client);
            context.HttpContext.Response.Headers.Add("uid", uid);
        }

        public void OnResourceExecuted(ResourceExecutedContext context)
        {
        }
    }
}