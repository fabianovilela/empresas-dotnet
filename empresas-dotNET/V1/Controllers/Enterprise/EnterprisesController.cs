﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using empresas_dotNET.V1.Authentication.Models;
using empresas_dotNET.V1.Enterprise.Filters;
using empresas_dotNET.V1.Enterprise.Models;
using empresas_dotNET.V1.Enterprise.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace empresas_dotNET.V1.Enterprise.Controllers
{
    [ApiController]
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class EnterprisesController : ControllerBase
    {
        private readonly IEnterpriseService _service;

        public EnterprisesController(IEnterpriseService enterpriseService)
        {
            _service = enterpriseService;
        }

        // GET: {{dev_host}}/api/{{api_version}}/enterprises?enterprise_types=1&name=aQm
        [HttpGet]
        [AddHeaders]
        public ActionResult<IEnumerable<Models.Enterprise>> Get(int enterprise_types, string name)
        {
            var isAuthorized = ResponseHeader.GetResponseHeader().IsAuthorized;

            if (isAuthorized)
            {
                Enterprise_Types type = (Enterprise_Types)enterprise_types;
                return _service.FilterAsync(name, type).ToList();
            } else
            {
                return BadRequest(StatusCodes.Status401Unauthorized);
            }
        }

        // GET: {{dev_host}}/api/{{api_version}}/enterprises/1
        [HttpGet("{id:int}")]
        [AddHeaders]
        public async Task<IActionResult> GetEnterprise([FromRoute] int id)
        {
            var isAuthorized = ResponseHeader.GetResponseHeader().IsAuthorized;

            if (isAuthorized)
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var enterprise = await _service.GetEnterpriseAsync(id);

                if (enterprise == null)
                {
                    return NotFound();
                }

                return Ok(enterprise);
            } else
            {
                return BadRequest(StatusCodes.Status401Unauthorized);
            }

        }

        private bool EnterpriseExists(int id)
        {
            return _service.ExistsAsync(id);
        }
    }
}