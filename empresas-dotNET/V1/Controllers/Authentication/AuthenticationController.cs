﻿using System.Threading.Tasks;
using empresas_dotNET.V1.Authentication.Models;
using empresas_dotNET.V1.Authentication.Services;
using Microsoft.AspNetCore.Mvc;

namespace empresas_dotNET.V1.Authentication.Controllers
{
    [ApiController]
    [ApiVersion("1")]
    public class AuthenticationController : ControllerBase
    {
        private readonly IAuthenticationService _service;
        
        public AuthenticationController(IAuthenticationService enterpriseService)
        {
            _service = enterpriseService;
        }

        // POST: {{dev_host}}/api/{{api_version}}/users/auth/sign_in
        [HttpPost]
        [Route("api/v{version:apiVersion}/users/auth/sign_in")]
        public async Task SignIn([FromBody] Credentials value)
        {
            var email = value.Email;
            var password = value.Password;

            await _service.SignIn(email, password);
        }
    }
}
