﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

//namespace empresas_dotNET1.Models
    namespace empresas_dotNET.V1.Enterprise.Models
{
    public class EnterpriseContext : DbContext
    {
        public EnterpriseContext (DbContextOptions<EnterpriseContext> options)
            : base(options)
        {
        }

        public DbSet<Enterprise> Enterprise { get; set; }
    }
}
