﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using empresas_dotNET.V1.Enterprise.Models;

namespace empresas_dotNET.Migrations
{
    [DbContext(typeof(EnterpriseContext))]
    [Migration("20181128025646_MysqlMigration")]
    partial class MysqlMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.4-rtm-31024")
                .HasAnnotation("Relational:MaxIdentifierLength", 64);

            modelBuilder.Entity("empresas_dotNET.V1.Enterprise.Models.Enterprise", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address");

                    b.Property<string>("Description");

                    b.Property<string>("Email");

                    b.Property<string>("Name");

                    b.Property<string>("PhoneNumber");

                    b.Property<int>("Type");

                    b.HasKey("Id");

                    b.ToTable("Enterprise");
                });
#pragma warning restore 612, 618
        }
    }
}
