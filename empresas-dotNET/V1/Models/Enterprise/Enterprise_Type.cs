﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace empresas_dotNET.V1.Enterprise.Models
{
    public enum Enterprise_Types : int
    {
        [Display(Name = "Agro")]
        Agro = 1,

        [Display(Name = "Aviation")]
        Aviation = 2,

        [Display(Name = "Biotech")]
        Biotech = 3,

        [Display(Name = "Eco")]
        Eco = 4,

        [Display(Name = "Ecommerce")]
        Ecommerce = 5,

        [Display(Name = "Education")]
        Education = 6,

        [Display(Name = "Fashion")]
        Fashion = 7,

        [Display(Name = "Fintech")]
        Fintech = 8,

        [Display(Name = "Food")]
        Food = 9,

        [Display(Name = "Games")]
        Games = 10,

        [Display(Name = "Health")]
        Health = 11,

        [Display(Name = "IOT")]
        IOT = 12,

        [Display(Name = "Logistics")]
        Logistics = 13,

        [Display(Name = "Media")]
        Media = 14,

        [Display(Name = "Mining")]
        Mining = 15,

        [Display(Name = "Products")]
        Products = 16,

        [Display(Name = "Real Estate")]
        RealEstate = 17,

        [Display(Name = "Service")]
        Service = 18,

        [Display(Name = "Smart City")]
        SmartCity = 19,

        [Display(Name = "Social")]
        Social = 20,

        [Display(Name = "Software")]
        Software = 21,

        [Display(Name = "Technology")]
        Technology = 22,

        [Display(Name = "Tourism")]
        Tourism = 23,

        [Display(Name = "Transport")]
        Transport = 24
    }
}

