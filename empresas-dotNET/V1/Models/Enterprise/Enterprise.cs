﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace empresas_dotNET.V1.Enterprise.Models
{
    public class Enterprise
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public Enterprise_Types Type { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
    }
}
