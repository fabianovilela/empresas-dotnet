﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace empresas_dotNET.V1.Enterprise.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var _context = new EnterpriseContext(
                serviceProvider.GetRequiredService<DbContextOptions<EnterpriseContext>>()))
            {
                // Look for any Enterprises.
                if (_context.Enterprise.Any())
                {
                    return;   // DB has been seeded
                }

                Random rand = new Random();
                int i = 1;

                while (i <= 20)
                {
                    var companyName = Faker.Company.Name();
                    _context.Enterprise.AddRange(
                        new Enterprise
                        {
                            Name = companyName,
                            Description = Faker.Lorem.Sentence(),
                            Type = (Enterprise_Types)rand.Next(1, 25),
                            Email = Faker.Internet.Email(companyName),
                            PhoneNumber = Faker.Phone.Number(),
                            Address = Faker.Address.StreetAddress() + " " + Faker.Address.City() + ", " + Faker.Address.UsStateAbbr() + " - " + Faker.Address.ZipCode() + " - " + Faker.Address.Country()
                        }
                    );

                    i++;
                }

                _context.SaveChanges();
            }
        }
    }
}
