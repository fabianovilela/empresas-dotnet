﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace empresas_dotNET.V1.Authentication.Models
{
    public class ResponseHeader
    {
        public string AccessToken { get; set; }
        public string Client { get; set; }
        public string Uid { get; set; }
        public bool IsAuthorized { get; set; } = false;

        private static ResponseHeader instance = new ResponseHeader();

        public static ResponseHeader GetResponseHeader()
        {
            return (instance == null) ? instance = new ResponseHeader() : instance;
        }

        private ResponseHeader() { }
    }
}
