﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace empresas_dotNET.V1.Authentication.Models
{
    public class Credentials
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
