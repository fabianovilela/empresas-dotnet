﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using empresas_dotNET.V1.Enterprise.Models;
using empresas_dotNET.V1.Enterprise.Services;
using empresas_dotNET.V1.Authentication.Services;

namespace empresas_dotNET
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddDbContext<EnterpriseContext>(options =>
                    options.UseMySql(Configuration.GetConnectionString("MySqlConnectionString")));

            services.AddApiVersioning();

            services.AddTransient<IEnterpriseService, EnterpriseService> ();
            services.AddTransient<IAuthenticationService, AuthenticationService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
                app.UseCors(builder =>
                    builder.AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowCredentials()
                        .WithHeaders("access-token", "client", "uid")
                        .WithExposedHeaders("access-token", "client", "uid")
                );
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
